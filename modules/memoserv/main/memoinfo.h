/*
 * Anope IRC Services
 *
 * Copyright (C) 2015-2017 Anope Team <team@anope.org>
 *
 * This file is part of Anope. Anope is free software; you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software
 * Foundation, version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see see <http://www.gnu.org/licenses/>.
 */

#include "modules/memoserv.h"

class MemoInfoImpl : public MemoServ::MemoInfo
{
	friend class MemoInfoType;

	Serialize::Storage<NickServ::Account *> account;
	Serialize::Storage<ChanServ::Channel *> channel;
	Serialize::Storage<int16_t> memomax;
	Serialize::Storage<bool> hardmax;

 public:
	using MemoServ::MemoInfo::MemoInfo;

	MemoServ::Memo *GetMemo(unsigned index) override;
	unsigned GetIndex(MemoServ::Memo *m) override;
	void Del(unsigned index) override;
	bool HasIgnore(User *u) override;

	NickServ::Account *GetAccount() override;
	void SetAccount(NickServ::Account *) override;

	ChanServ::Channel *GetChannel() override;
	void SetChannel(ChanServ::Channel *) override;

	int16_t GetMemoMax() override;
	void SetMemoMax(const int16_t &) override;

	bool IsHardMax() override;
	void SetHardMax(bool) override;

	std::vector<MemoServ::Memo *> GetMemos() override;
	std::vector<MemoServ::Ignore *> GetIgnores() override;
};

