/*
 * Anope IRC Services
 *
 * Copyright (C) 2005-2017 Anope Team <team@anope.org>
 *
 * This file is part of Anope. Anope is free software; you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software
 * Foundation, version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "anope.h"
#include "base.h"
#include "bots.h"
#include "channels.h"
#include "commands.h"
#include "config.h"
#include "event.h"
#include "extensible.h"
#include "hashcomp.h"
#include "language.h"
#include "lists.h"
#include "logger.h"
#include "mail.h"
#include "messages.h"
#include "modes.h"
#include "modules.h"
#include "numeric.h"
#include "opertype.h"
#include "protocol.h"
#include "serialize.h"
#include "servers.h"
#include "service.h"
#include "services.h"
#include "socketengine.h"
#include "sockets.h"
#include "threadengine.h"
#include "timers.h"
#include "uplink.h"
#include "users.h"
#include "xline.h"

#include "modules/chanserv.h"
#include "modules/nickserv.h"
#include "modules/botserv.h"
#include "modules/memoserv.h"
#include "modules/hostserv.h"
#include "accessgroup.h"
